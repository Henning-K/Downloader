package com.example.downloader;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.webkit.URLUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloaderService extends Service {

    public static final int UPDATE_PROGRESS = 456789;
    public static final int FINISH_PROGRESS = 123789;

    String download_url;
    private int percentage;
    String full_path;
    String file_name;

    void updateNotification(String text) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, getString(R.string.channel_id))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Downloader")
                .setContentText(text)
                .setContentIntent(pendingIntent).build();
        startForeground(1024, notification);
    }

    @Override
    public void onCreate() {
        updateNotification("Standby");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        updateNotification("Downloading...");
        download_url = intent.getStringExtra("url");
//        final LocalBroadcastManager mng = LocalBroadcastManager.getInstance(this);
        this.percentage = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(download_url);
                    URLConnection connection = url.openConnection();
                    connection.connect();

                    long file_len = connection.getContentLengthLong();
                    InputStream input = new BufferedInputStream(connection.getInputStream());
                    file_name = URLUtil.guessFileName(url.toString(), null, null);
                    File output_file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), file_name);
                    full_path = output_file.toString();
                    FileOutputStream output = new FileOutputStream(output_file);

                    byte[] data = new byte[1024];
                    long total = 0;
                    long current;
                    while ((current = input.read(data)) != -1) {
                        total += current;

                        int percentage_ = (int) ((total * 100) / (file_len));
                        if (percentage_ > percentage) {
                            Log.d(this.toString(), String.format("in service: %d (%d/%d)", percentage, total, file_len));
                            Intent broad_intent = new Intent(MainActivity.ACTION_UPDATE)
                                    .putExtra("progress", percentage);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(broad_intent);
                        }
                        percentage = percentage_;
                        output.write(data);

                    }

                    output.flush();
                    output.close();
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Intent broad_intent = new Intent(MainActivity.ACTION_FINISH)
                        .putExtra("file_name", file_name);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(broad_intent);

            }
        }).start();


        return START_STICKY;
    }
}
