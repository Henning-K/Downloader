package com.example.downloader;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements OnRequestPermissionsResultCallback {


    String download_link;
    ProgressBar download_progress;

    private static final int REQUEST_WRITE_EXTERNAL = 0;
    private static final int REQUEST_READ_EXTERNAL = 1;
    private static final int REQUEST_INTERNET = 2;
    private static final int REQUEST_FOREGROUND = 3;
    public static final String ACTION_UPDATE = "update.downloader.hekowa";
    public static final String ACTION_FINISH = "finish.downloader.hekowa";

    LocalBroadcastManager mng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        createNotificationChannel();
        super.onCreate(savedInstanceState);
        download_progress =

                findViewById(R.id.dlProgress);

    }

    private BroadcastReceiver receiver;

    private boolean verifyPermissions(int[] grantResults) {
        if (grantResults.length < 1) {
            return false;
        }

        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_EXTERNAL:
                Log.i(this.toString(), "Received response for WRITE_EXTERNAL_STORAGE request.");
                if (this.verifyPermissions(grantResults)) {
                    Log.i(this.toString(), "WRITE_EXTERNAL_STORAGE permission granted.");
                } else {
                    Log.i(this.toString(), "WRITE_EXTERNAL_STORAGE permission not granted.");
                }
                break;
            case REQUEST_READ_EXTERNAL:
                Log.i(this.toString(), "Received response for READ_EXTERNAL_STORAGE request.");
                if (this.verifyPermissions(grantResults)) {
                    Log.i(this.toString(), "READ_EXTERNAL_STORAGE permission granted.");
                } else {
                    Log.i(this.toString(), "READ_EXTERNAL_STORAGE permission not granted.");
                }
                break;
            case REQUEST_INTERNET:
                Log.i(this.toString(), "Received response for INTERNET request.");
                if (this.verifyPermissions(grantResults)) {
                    Log.i(this.toString(), "INTERNET permission granted.");
                } else {
                    Log.i(this.toString(), "INTERNET permission not granted.");
                }
                break;
            case REQUEST_FOREGROUND:
                Log.i(this.toString(), "Received response for FOREGROUND request.");
                if (this.verifyPermissions(grantResults)) {
                    Log.i(this.toString(), "FOREGROUND permission granted.");
                } else {
                    Log.i(this.toString(), "FOREGROUND permission not granted.");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void do_download(View view) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, REQUEST_INTERNET);
        }


        download_link = ((EditText) findViewById(R.id.dlInput)).getText().toString();

        Intent intent = new Intent(this, DownloaderService.class);
        intent.putExtra("url", download_link);

        getApplicationContext().startForegroundService(intent);
    }

    // https://developer.android.com/training/notify-user/build-notification#Priority
    private void createNotificationChannel() {
        CharSequence name = getString(R.string.channel_name);
        String description = getString(R.string.channel_description);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel channel = new NotificationChannel(getString(R.string.channel_id), name, importance);
        channel.setDescription(description);
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
    }


    @Override
    protected void onDestroy() {
        mng.unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Filter
                if (ACTION_UPDATE.equals(intent.getAction())) {
                    int progress = intent.getIntExtra("progress", 0);
                    download_progress.setProgress(progress, true);
                } else if (ACTION_FINISH.equals(intent.getAction())) {
                    String file_name = intent.getStringExtra("file_name");
                    Toast.makeText(getApplicationContext(), "Download finished " + file_name,
                            Toast.LENGTH_LONG).show();
                    stopService(new Intent(getApplicationContext(), DownloaderService.class));
                }
            }
        };

        Log.i("MainActivity", "1");
        mng = LocalBroadcastManager.getInstance(this);
        Log.i("MainActivity", "2");
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_UPDATE);
        filter.addAction(ACTION_FINISH);
        Log.i("MainActivity", "3");
        mng.registerReceiver(receiver, filter);
        Log.i("MainActivity", "4");
    }
}